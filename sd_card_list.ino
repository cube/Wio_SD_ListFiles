#include "TFT_eSPI.h"
#include <SPI.h>
#include <Seeed_FS.h>
#include "SD/Seeed_SD.h"

TFT_eSPI tft;

void setup() {
  // Set up the display
  tft.begin();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  tft.setFreeFont(&FreeSans9pt7b);

  // Print the title
  tft.setTextColor(TFT_GREENYELLOW);
  tft.drawString("SD Card", 10, 10);

  // Display status
  tft.setTextColor(TFT_WHITE);
  tft.drawString("Initializing SD Card...", 10, 30);

  // Set up the SD card
  if(!SD.begin(SDCARD_SS_PIN, SDCARD_SPI))
  {
    // Stay here if it fails; display an error message
    tft.setTextColor(TFT_RED);
    tft.drawString("SD Initialization Failed!", 10, 50);
    // This is more than likely the reason on this kind of board. 
    tft.drawString("Ensure SD card is inserted.", 10, 60);
    while(1){}
  }

  // Succes :D
  tft.drawString("SD Card Initialized", 10, 50);
  delay(2000);

  // Blank the screen, print a title
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_GREENYELLOW);
  tft.drawString("Directories and files in root", 10, 10);
  tft.setTextColor(TFT_WHITE);

  // We only scan the root directory at the minute
  File dir = SD.open("/");
  int textpos = 30; // Starting position for lines of text
  while(true)
  {
    File entry = dir.openNextFile();
    if(!entry){break;} // None entry breaks the loop

    if(entry.isDirectory()){tft.setTextColor(TFT_BLUE);} // Directories print in blue
    else{tft.setTextColor(TFT_WHITE);} // Files print in white

    tft.drawString(entry.name(), 10, textpos); // Print the directory/file to the screen
    textpos += 20; // Increase the line position for the next line
    entry.close(); // Clean up
  }
}

void loop() {}
