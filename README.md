# README #
Lists files in the root directory of an SD card in the Seeed Wio Terminal, on the built in display.

Directories are printed in blue font, while files are simply white.

### Todo ###
- If there are more than 7 files and directories, they will not all fit on the screen. Add pagination to allow the user to use the directional keys on the terminal to view the next and previous pages of files.
- Display files within directories, create functions to help.
